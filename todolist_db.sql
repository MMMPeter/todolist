-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 01, 2018 at 05:58 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `todolist_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `acc_file`
--

CREATE TABLE `acc_file` (
  `acc_id` int(50) NOT NULL,
  `acc_name` varchar(100) NOT NULL,
  `acc_email` varchar(100) NOT NULL,
  `acc_pw` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `acc_file`
--

INSERT INTO `acc_file` (`acc_id`, `acc_name`, `acc_email`, `acc_pw`) VALUES
(1, 'Jhon', 'jhon@gmail.com', 'jhonpw'),
(2, 'Peter', 'peter@gmail.com', 'peterpw'),
(4, 'Nancy', 'nancy', 'nancypw'),
(5, 'gay@gmail.com', 'gay@gmail.com', 'gaypw'),
(6, 'KiKi', 'kiki@gmail.com', 'kikipw');

-- --------------------------------------------------------

--
-- Table structure for table `task_file`
--

CREATE TABLE `task_file` (
  `task_id` int(50) NOT NULL,
  `task_name` varchar(100) NOT NULL,
  `task_st_date` date NOT NULL,
  `task_en_date` date NOT NULL,
  `task_status` varchar(50) NOT NULL,
  `by_acc_id` int(50) NOT NULL,
  `task_des` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `task_file`
--

INSERT INTO `task_file` (`task_id`, `task_name`, `task_st_date`, `task_en_date`, `task_status`, `by_acc_id`, `task_des`) VALUES
(1, 'Go to Kyaw\'s house', '2018-10-04', '0000-00-00', 'DONE', 5, 'ddd'),
(2, 'Buy Cat food', '2018-10-11', '2018-10-11', 'DONE', 5, ''),
(3, 'Make To Do list website', '2018-10-01', '2018-10-01', 'NotDone', 0, 'TO make CRUD php website'),
(4, 'Make To Do list website ok', '2018-10-01', '2018-10-01', 'Make To Do list website', 5, 'TO make CRUD php website yet'),
(5, 'Water Bonsin Plant', '2018-10-05', '2018-10-06', 'NotDone', 5, 'Importance'),
(6, 'test', '2018-10-10', '2018-10-18', 'DONE', 5, 'ste'),
(7, 'test', '2018-10-10', '2018-10-18', 'DONE', 5, 'ste'),
(9, 'Let have dinner with old friends', '2018-10-19', '0000-00-00', 'NotDone', 5, 'importance'),
(10, 'asdsdaf', '0000-00-00', '0000-00-00', '', 5, 'asdsdaf'),
(12, 'Mornee walk weekend', '2018-10-20', '0000-00-00', 'NotDone', 6, 'cool');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acc_file`
--
ALTER TABLE `acc_file`
  ADD PRIMARY KEY (`acc_id`),
  ADD UNIQUE KEY `acc_email` (`acc_email`);

--
-- Indexes for table `task_file`
--
ALTER TABLE `task_file`
  ADD PRIMARY KEY (`task_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acc_file`
--
ALTER TABLE `acc_file`
  MODIFY `acc_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `task_file`
--
ALTER TABLE `task_file`
  MODIFY `task_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
