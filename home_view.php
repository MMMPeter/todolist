<?php
  $userId = $_SESSION["accId"];
?>
<!DOCTYPE html>
<html>
<head>
  <title>Login</title>
<meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="css/util.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="css/w3.css">
<!--===============================================================================================-->
</head>
<body>
  <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="#">ToDoList</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
     
      <ul class="nav navbar-nav navbar-right">        
        <li>
          <form action="" method="POST"> <button class="btn btn-sm btn-warning" style="margin: 10px;" type="submit" name="logout"><span class="glyphicon glyphicon-log-out"></span> Logout</button></form>
        </li>
      </ul>
    </div>
  </div>
</nav>

  <?php

    /*create teacher */
   
    if (isset($_POST['save'])) 
    {
       $byAccId = $_SESSION['accId'];
         $tkIda = $_POST['tkId'];
      $tkDes = mysqli_real_escape_string( $conn,$_POST['tkDes']);
       $tkTitle = mysqli_real_escape_string( $conn,$_POST['tkName']);
      $stDate = $_POST['stDate'];
      $enDate = $_POST['enDate'];

         $sqlua = "UPDATE task_file SET task_name='$tkTitle', task_des='$tkDes', task_st_date='$stDate', task_en_date='$enDate' , by_acc_id ='$byAccId' WHERE task_id='$tkIda' ";

          if (mysqli_query($conn, $sqlua)) {
               header("Refresh:0; url=http://localhost/todolist/");
          } else {
              echo "Error updating record: " . mysqli_error($conn);
          }


    }elseif (isset($_POST['createTsk'])) 
    {
      $tkTitle = mysqli_real_escape_string( $conn,$_POST['tkTitle']);
      $tkDes = mysqli_real_escape_string( $conn,$_POST['tkDes']);
      $stDate = $_POST['stDate'];
      $enDate = $_POST['enDate'];

      $sql = "INSERT INTO task_file (task_name, task_st_date,task_en_date, task_status, by_acc_id, task_des)
      VALUES ('$tkTitle', '$stDate', '$enDate', 'NotDone', '$userId', '$tkDes')";

      if ($conn->query($sql) === TRUE) {

        header("Refresh:0; url=http://localhost/todolist/");
      } else {
         $errMsg =  "Error: " . $sql . "<br>" . $conn->error;
      }
/*end create teacher*/
    }elseif (isset($_POST['addDone'])) 
    {
      $tkId = $_POST['tkId'];
    
    $sqlu = "UPDATE task_file SET task_status='DONE' WHERE task_id='$tkId' ";

      if (mysqli_query($conn, $sqlu)) {
           header("Refresh:0; url=http://localhost/todolist/");
      } else {
          echo "Error updating record: " . mysqli_error($conn);
      }
    }elseif (isset($_POST['edit'])) 
    {
      $tkId = $_POST['tkId'];
            $sqlNf = "SELECT * FROM task_file WHERE task_id='$tkId'";
                $postList = mysqli_query($conn,$sqlNf);

                /*start anno edit*/
                while($res = mysqli_fetch_array($postList)) 
                    {
                    $taskId = $res['task_id'];
                    $taskName = $res['task_name'];
                    $taskStDate = $res['task_st_date'];
                    $taskEnDate = $res['task_en_date'];
                    $taskStatus= $res['task_status'];
                    $byAccId= $res['by_acc_id'];
                    $taskDes= $res['task_des'];
                
               
            ?>

            

            <div style="padding-left: 200px;padding-right: 200px;padding-top: 30px;">
              <form action='' method='POST'  >
                <input type="text" name="tkId"  value="<?php echo($taskId); ?>" style='display: none;' >
                <label for="taskName">Task Title : </label>
                  <input type="text" name="tkName" id="taskName" value="<?php echo($taskName); ?>" class='form-control'><br>

                  <label for="taskDes">Task Description : </label>
                  <textarea type="text" name="tkDes" id="taskDes" class='form-control'><?php echo($taskDes); ?></textarea>
                  <br>
                     <label for="stDate">Start Date : </label>
                  <input class="form-control" name="stDate" type="date" value="<?php echo($taskStDate) ?>" placeholder="Select Start Date"/>
                  <br>
                  <label for="enDate">Deadline : </label>
                  <input type="date" name="enDate" class="form-control" value="<?php echo($taskEnDate) ?>"  placeholder="Select Deadline" >
                  <br>

                

                <input type="submit" name="cancel" value="CANCEL" style="width: 48%;margin-left: 10px;" class="btn btn-md btn-warning">
                <input type="submit" name="save" value="SAVE" style="width: 48%;margin-left: 10px; " class="btn btn-md btn-info">
              </form>
              
            </div>

            <?php
            }
            /*end anno edit*/  
    }elseif (isset($_POST['del'])) 
    {
      $tkId = $_POST['tkId'];
      $sqld = "DELETE FROM task_file WHERE task_id='$tkId'";

      if ($conn->query($sqld) === TRUE) {
            header("Refresh:0; url=http://localhost/todolist/");
      } else {
        echo "Error deleting record: " . $conn->error;
      }
    }elseif (isset($_POST['viewDetail'])) 
    {
      $tkId = $_POST['tkId'];
      ?>

<div class="container" style="background-color: #5c5c3d;">
  <?php

      $sqlNf = "SELECT * FROM task_file WHERE task_id ='$tkId' ";
      $postList = mysqli_query($conn,$sqlNf);
      $tempNo = 0;
      while($res = mysqli_fetch_array($postList)) 
          {
            $tempNo = $tempNo +1;
          
          $annoId = $res['task_id'];
          $annoName = $res['task_name'];
          $annoDes = $res['task_des'];
          $annoStDate = $res['task_st_date'];
          $annoEnDate= $res['task_en_date'];
          $annoStatus = $res['task_status'];
          $annoById = $res['by_acc_id'];


      ?>

  <div>  <form action="" method="POST"> <button type="submit" name="cancel"  class="btn btn-sm btn-default" style="margin: 5px;">CANCEL</button>
  </form>

        <ul  style="list-style:   none;padding: 50px;">
        

          <li  class="liItemDetail">
            <div>
                  <div style="width: 100%;float: left;padding-left: 50px;height: 100%">
                         <h5 class="well"><b><?php echo $annoName; ?></b> </h5>
                         <p style="color: white;font-size: 16px; font-family: 'Times New Roman', 'Times', 'serif';">
                  <?php
                    echo  $tempNo .". ".$annoName."<br> Start Date : ".$annoStDate."<br> Deadline : ".$annoEnDate."<br> Description : ".$annoDes
                    ;
                  ?>
                </p>
                  </div>
                  <div style="width: 300px;float: right;height: 20%;">
                    <form action="" method="POST">
                      <input type="number" name="tkId" value="<?php echo$annoId; ?>" style='display: none;'>
                    <button type="submit"  name="addDone" class="btn btn-sm btn-info" style="width: 100%;height: 30px;margin-bottom: 5px;">DONE</button>
           
                  <br>

                  <button type="submit" name="edit" class="btn btn-sm btn-warning" style="width: 100%;height: 30px;margin-bottom: 5px;">Edit</a>
                  <br>
                   

                  <button type="submit" name="del" class="btn btn-sm btn-danger" style="width: 100%;height: 30px;margin-bottom: 5px;">Delete</a>
                  <br>

                   <button type="submit" name="cancel"  class="btn btn-sm btn-default" style="width: 100%;height: 30px;margin-bottom: 5px;">CANCEL</button>
                 </form>
                  </div>


            </div>
          </li>
                    
        </ul>   
      </div>

            <?php   
              
          }
            /*end defaul showlist*/
       

    }elseif (isset($_POST['cancel'])) 
    {
       header("Refresh:0; url=http://localhost/todolist/");

    } else
    {    

    ?>


<div class="addTask">
     <button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#myModalTec" style="margin:5px;">Create New Task</button>
</div>

<div class="container">
  <?php

      $sqlNf = "SELECT * FROM task_file WHERE by_acc_id ='$userId'  AND task_status='NotDone' ORDER BY task_st_date ASC ";
      $postList = mysqli_query($conn,$sqlNf);
      $tempNo = 0;
      while($res = mysqli_fetch_array($postList)) 
          {
            $tempNo = $tempNo +1;
          
          $annoId = $res['task_id'];
          $annoName = $res['task_name'];
          $annoDes = $res['task_des'];
          $annoStDate = $res['task_st_date'];
          $annoEnDate= $res['task_en_date'];
          $annoStatus = $res['task_status'];
          $annoById = $res['by_acc_id'];


      ?>

  <div >

        <ul class='list-group' style="list-style:   none;">
          <li  class="liItem">
            <div >
              
                            
                  <div style="width: 80%;float: left;padding-left: 50px;">
                         <h5 class="card-title"><b><?php echo $annoName; ?></b> </h5>

                  <?php
                    echo  $tempNo .". ".$annoName."<br> Start Date : ".$annoStDate."<br> Deadline : ".$annoEnDate
                    ;
                  ?>
                  </div>
                  <div style="width: 20%;float: right;height: 100%;">
                    <form action="" method="POST">
                      <input type="number" name="tkId" value="<?php echo$annoId; ?>" style='display: none;'>
                    <button type="submit"  name="addDone" class="btn btn-sm btn-info" style="width: 100%;height: 30px;margin-bottom: 5px;">DONE</button>
           
                  <br>

                  <button type="submit" name="edit" class="btn btn-sm btn-warning" style="width: 100%;height: 30px;margin-bottom: 5px;">Edit</a>
                  <br>
                   

                  <button type="submit" name="del" class="btn btn-sm btn-danger" style="width: 100%;height: 30px;margin-bottom: 5px;">Delete</a>
                  <br>

                   <button type="submit" name="viewDetail"  class="btn btn-sm btn-link" style="width: 100%;height: 30px;margin-bottom: 5px;">Detail......</button>
                 </form>
                  </div>


            </div>
          </li>
                    
        </ul>   
      </div>

      <?php   
        
    }
      /*end defaul showlist*/

    }/*end else*/
      ?>
        
</div>


    <!-- Modal -->
    <div id="myModalTec" class="modal fade" role="dialog">
      <div class="modal-dialog">

       <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Create New Task</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            
          </div>
          <div class="modal-body">
            <form action="" method="POST">
            
            <br>
            <label for="tkTitle">Task Title: </label>
            <input type="text" name="tkTitle" class="form-control" placeholder="Enter Task Title">
            <br>
            <label for="tkDes">Task Description: </label>
            <textarea  name="tkDes" class="form-control" placeholder="Enter Task Description" style="max-width: 100%;min-width: 100%;max-height: 120px;min-height: 120px;"></textarea>
            <br>
            <label for="stDate">Start Date : </label>
            <input class="form-control" name="stDate" type="date"  placeholder="Select Start Date"/>
            <br>
            <label for="enDate">Deadline : </label>
            <input type="date" name="enDate" class="form-control" placeholder="Select Deadline" >
            <br>

          
            
          </div>
          <div class="modal-footer" style="width: 100%;">

            <button type="button" class="btn btn-success" data-dismiss="modal" >Cancel
            </button >
            <button type="submit" class="btn btn-info" " name="createTsk">Create  
            </button >
            </form>
          </div>
        </div>

      </div>
    </div>
      
    </div>  
    <!-- model end -->


<!--===============================================================================================-->
  <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
  <script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
  <script src="vendor/bootstrap/js/popper.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
  <script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
  <script src="vendor/daterangepicker/moment.min.js"></script>
  <script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
  <script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
  <script src="js/main.js"></script>
</body>
</html>