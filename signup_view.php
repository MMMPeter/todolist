<?php
  $err ='';
if ($result =='Your email is already used!!' ) 
{
 $err = 'Email alreay Exist';
}
?>

<!DOCTYPE html>
<html>
<head>
  <title></title>
<meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="css/util.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="css/w3.css">
<!--===============================================================================================-->
</head>
<body>

  <div  class="well">
    Make your To Do List Now
  </div>
    <div class="" style="padding-left: 400px;padding-right: 400px;padding-top: 2px;">
    <div class="wrapper">
    <form class="form-signin" action="" method="POST">
      <h2 class="form-signin-heading">Sign Up your account</h2><br>
      <input type="text" class="form-control" name="newName" placeholder="User Name" required="" autofocus="" /> <br>
      <input type="text" class="form-control" name="newEmail" placeholder="Email Address" required="" autofocus="" /> <br>
      <input type="password" class="form-control" name="newPassword" placeholder="Password" required=""/>      <br>
      
      <span style="color: red;">
      <?php echo $err; ?>
      </span>
      <br>

      <button class="btn btn-lg btn-primary btn-block" name="signUp" value="signUp" type="submit">Sign Up</button>   
    </form>
  </div> <!-- end wrapper -->

<form action="" method="POST">
    <button type='submit' name='startLogin' value="login" class="btn btn-md btn-link" style="margin-left: 2px; float: left;margin-top: 5px;"><span>Login</span></button> 
</form>

</div><!-- end container -->



<!--===============================================================================================-->
  <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
  <script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
  <script src="vendor/bootstrap/js/popper.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
  <script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
  <script src="vendor/daterangepicker/moment.min.js"></script>
  <script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
  <script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
  <script src="js/main.js"></script>
</body>
</html>