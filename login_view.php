
<!DOCTYPE html>
<html>
<head>
  <title>ToDoList</title>
<meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="css/util.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="css/w3.css">
<!--===============================================================================================-->
</head>
<body>

  <?php  $err ='';
if (isset($result)) 
{
 $err = $result;
}
?>

  <div  class="well">
    Make your To Do List Now
  </div>
    <div  style="padding-left: 400px;padding-right: 400px;padding-top: 50px;">
    <div class="wrapper">
    <form class="form-signin" action="" method="POST">
      <h2 class="form-signin-heading">Please login</h2><br>
      <input type="text" class="form-control" name="email" placeholder="Email Address" required="" autofocus="" /> <br>
      <input type="password" class="form-control" name="password" placeholder="Password" required=""/>      <br>
      
      <button class="btn btn-lg btn-primary btn-block" name="login" type="submit">Login</button>   
    </form>
  </div> <!-- end wrapper -->

<form action="" method="POST">
   <button type='submit' name='startSignUp' value="SignUp" class="btn btn-md btn-link" style="margin-left: 2px; float: left;margin-top: 5px;"><span>SignUp</span></button> 
</form>

</div><!-- end container -->



<!--===============================================================================================-->
  <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
  <script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
  <script src="vendor/bootstrap/js/popper.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
  <script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
  <script src="vendor/daterangepicker/moment.min.js"></script>
  <script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
  <script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
  <script src="js/main.js"></script>
</body>
</html>